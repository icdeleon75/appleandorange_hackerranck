import java.util.ArrayList;
import java.util.List;

public class Principal {

    public static void main(String[] args){

        List<Integer> apple = new ArrayList<Integer>();
        apple.add(2);
        apple.add(3);
        apple.add(-4);

        List<Integer> orange = new ArrayList<Integer>();
        orange.add(3);
        orange.add(-2);
        orange.add(-4);

        countApplesAndOranges(7,10,4,12, apple, orange);
    }

    private static void countApplesAndOranges(int s, int t, int a, int b, List<Integer> apples, List<Integer> oranges) {
        // Write your code here
        short appleCounter =0;
        short orangeCounter =0;
        for (int i=0; i< apples.size(); i++){
            if((a + apples.get(i)) >= s && (a + apples.get(i)) <= t){
                appleCounter++;
            }
        }
        for (int i=0; i< oranges.size(); i++){
            if((b + oranges.get(i)) >= s && (b + oranges.get(i)) <= t){
                orangeCounter++;
            }
        }
        System.out.println(appleCounter + "\n" + orangeCounter);
    }
}
